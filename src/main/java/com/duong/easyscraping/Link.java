package com.duong.easyscraping;

import java.time.LocalDateTime;
import java.util.Optional;

/**
 * Represent a link.
 * 
 * @author duongnt.is@gmail.com
 */
public interface Link {
	/**
	 * @return absolute location of the link.
	 */
	public String getUrl();
	
	/**
	 * @return title of the link.
	 */
	public Optional<String> getTitle();
	
	/**
	 * @return version date of the link, normally the updated date, used to identify of a link has changed.
	 */
	public Optional<LocalDateTime> version();
}
