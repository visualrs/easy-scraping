package com.duong.easyscraping.utilities;

import java.io.StringWriter;
import java.util.Collection;
import java.util.Map;
import java.util.Optional;
import java.util.function.Function;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import javax.script.ScriptContext;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;

import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Utilities to deal with JSoup documents.
 * 
 * @author duongnt.is@gmail.com
 */
public class Documents
{
    private static final Logger LOGGER = LoggerFactory.getLogger(Documents.class);

    public static String extractText(final Element document, final String cssQuery)
    {
        String text = extractOne(document, cssQuery, Element::text);
        LOGGER.debug("Extract text from [{}], result: [{}]", cssQuery, text);
        return text;
    }

    public static String extractInnerHtml(final Element document, final String cssQuery)
    {
        String text = extractOne(document, cssQuery, Element::html);
        LOGGER.debug("Extract inner html from [{}], result: [{}]", cssQuery, text);
        return text;
    }

    public static String extractAttr(final Element document, final String cssQuery, final String attr)
    {
        String text = extractOne(document, cssQuery, element -> element.attr(attr));
        LOGGER.debug("Extract [{}] from [{}], result: [{}]", attr, cssQuery, text);
        return text;
    }

    public static String extractOne(final Element document, final String cssQuery, final Function<Element, String> elementExtractor)
    {
        String value = null;
        Elements elements = document.select(cssQuery);
        if (!elements.isEmpty())
        {
            value = elementExtractor.apply(elements.first());
        }
        return value;
    }

    public static Collection<String> extractAll(final Element document, final String cssQuery, final Function<Element, String> elementExtractor)
    {
        Elements elements = document.select(cssQuery);
        return elements.stream().map(elementExtractor::apply).collect(Collectors.toList());
    }

    /**
     * Select item with enhanced CSS selector.
     * 
     * Selector include a extract method, which default is text.
     * Examples:
     *
     * <li><pre>div > a >> abs:href  => extract href</pre></li>
     * <li><pre>div > a >> abs:html  => extract innner html</pre></li>
     * <li><pre>div > a >> attr  => extract attr</pre></li>
     * <li><pre>div > a  => extract text</pre></li>
     */
    public static String extract(final Element document, final String selector)
    {
    	if (selector.contains(">>"))
		{
		    String[] split = selector.split(">>");
		    Assert.hasText(split[0]);
		    Assert.hasText(split[1]);
		    if ("html".equalsIgnoreCase(split[1].trim()))
		    {
		        return extractInnerHtml(document, split[0].trim());
		    }
		    else
		    {
		        return extractAttr(document, split[0].trim(), split[1].trim());
		    }
		}
		else
		{
		    return extractText(document, selector);
		}
    }

    /**
     * Selector include a extract method, which default is text.
     * Examples:
     *
     * <li><pre>.div .a >> abs:href  => extract href</pre></li>
     * <li><pre>.div .a >> abs:html  => extract innner html</pre></li>
     * <li><pre>.div .a >> attr  => extract attr</pre></li>
     * <li><pre>.div .a  => extract text</pre></li>
     * <li><pre>.div .a >> script  => execute the content as a script and get text</pre></li>
     */
    public static Collection<String> extractAll(final Element document, final String selector)
    {
    	if (selector.contains(">>"))
		{
		    String[] split = selector.split(">>");
		    Assert.hasText(split[0]);
		    Assert.hasText(split[1]);
		    if ("html".equalsIgnoreCase(split[1].trim()))
		    {
		        return extractAll(document, split[0].trim(), Element::html);
		    }
		    else
		    {
		        return extractAll(document, split[0].trim(), e -> e.attr(split[1].trim()));
		    }
		}
		else
		{
		    return extractAll(document, selector, Element::text);
		}
    }


    /**
     * Selector include a extract method, which default is text.
     * Examples:
     *
     * <li><pre>.div .a >> abs:href  => extract href</pre></li>
     * <li><pre>.div .a >> abs:html  => extract innner html</pre></li>
     * <li><pre>.div .a >> attr  => extract attr</pre></li>
     * <li><pre>.div .a  => extract text</pre></li>
     * <li><pre>.div .a >> script  => execute the content as a script and get text</pre></li>
     */
    private static Collection<String> extractAllWithoutPostProcess(final Element document, final String selector)
    {
        if (selector.contains(">>"))
        {
            String[] split = selector.split(">>");
            Assert.hasText(split[0]);
            Assert.hasText(split[1]);
            if ("html".equalsIgnoreCase(split[1].trim()))
            {
                return extractAll(document, split[0].trim(), Element::html);
            }
            else
            {
                return extractAll(document, split[0].trim(), e -> e.attr(split[1].trim()));
            }
        }
        else
        {
            return extractAll(document, selector, Element::text);
        }
    }

    public static <T> T extractWithPattern(Element element, String selector, Map<Pattern, T> patterns)
    {
        return extractWithPattern(element, selector, patterns, null);
    }

    public static <T> T extractWithPattern(Element element, String selector, Map<Pattern, T> patterns, T defaultValue)
    {
        String text = extractText(element, selector);
        Optional<T> map = patterns.entrySet().stream()
                                             .filter(entry -> entry.getKey().matcher(text).matches())
                                             .findFirst().map(Map.Entry::getValue);

        return map.isPresent() ? map.get() : defaultValue;
    }

}
