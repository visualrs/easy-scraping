package com.duong.easyscraping.utilities;

/**
 * We don't wanna depend on any libs like common-lang just for simple needs. So, reinvent the wheel.
 * 
 * @author duongnt.is@gmail.com
 */
public class Texts {
	private Texts() {}
	
	public static boolean isBlank(String v) {
		return !isNotBlank(v);
	}
	
	public static boolean isNotBlank(String v) {
		if (Texts.isEmpty(v)) {
			return false;
		}
		
		int strLen = v.length();
		for (int i = 0; i < strLen; i++) {
			if (!Character.isWhitespace(v.charAt(i))) {
				return true;
			}
		}
		return false;
	}
	
	public static boolean isNotEmpty(String v) {
		return !isEmpty(v);
	}

	public static boolean isEmpty(String v) {
		return v == null || v.isEmpty();
	}

}
