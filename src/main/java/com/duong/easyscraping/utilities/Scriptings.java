package com.duong.easyscraping.utilities;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.StringWriter;

import javax.script.ScriptContext;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;

/**
 * Equip the scripting abilities.
 * 
 * @author duongnt.is@gmail.com
 */
public class Scriptings {
	private static final Logger LOGGER = LoggerFactory.getLogger(Scriptings.class);
	
	public static void main(String[] args) {
		System.out.println(runScript("&#39640;&#32423;&#31243;&#24207;&#35774;&#35745;",
				"var decodeHtmlEntity = function(str) {return str.replace(/&#(\\d+);/g, function(match, dec) {return String.fromCharCode(dec);});};print(decodeHtmlEntity(value));"));
	}

	/**
	 * Run a Javascript that process an input value, via <code>value</code>
	 * variable and produce an output. The value can be return by return
	 * statement, or by printing to the output (e.g. <code>print()</code>)
	 * function.
	 * 
	 * @param inVal
	 *            the input value.
	 * @param script
	 *            the script to process the input value.
	 * @return the output value.
	 */
	public static String runScript(final String inVal, final String script) {
		ScriptEngine engine = new ScriptEngineManager().getEngineByName("javascript");
		ScriptContext context = engine.getContext();
		StringWriter writer = new StringWriter();
		context.setWriter(writer);

		engine.put("value", inVal);
		try {
			Object val = engine.eval(script);
			if (val != null) {
				return String.valueOf(val);
			}
		} catch (ScriptException e) {
			LOGGER.warn("Error evaluating the script [" + script + "] with value [" + inVal + "]" , e);
			return null;
		}

		return writer.toString();
	}
}
