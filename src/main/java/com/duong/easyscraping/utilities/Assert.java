package com.duong.easyscraping.utilities;

/**
 * @author duongnt.is@gmail.com
 */
public class Assert {

	public static void hasText(String v) {
		if (Texts.isBlank(v)) {
			throw new IllegalArgumentException("[Assertion failed] - this String argument must have text; it must not be null, empty, or blank");
		}
	}
	
}
