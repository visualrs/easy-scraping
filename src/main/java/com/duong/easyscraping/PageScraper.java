package com.duong.easyscraping;

import com.duong.easyscraping.exception.ScrapingException;

/**
 * @author duongnt.is@gmail.com
 */
public interface PageScraper {

	/**
	 * Scrap data from a given url.
	 * 
	 * @param url url to the page to scrap.
	 * @return extracted data in JSON format.
	 */
	String scrap(String url) throws ScrapingException;

	/**
	 * Scrap data from a given url.
	 *
	 * @param url url to the page to scrap.
	 * @return extracted data in intended object format.
	 */
	<T> T scrap(String url, Class<T> clazz) throws ScrapingException;
}
