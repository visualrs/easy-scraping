package com.duong.easyscraping.search;

import java.time.LocalDateTime;
import java.util.Optional;

import com.duong.easyscraping.Link;

/**
 * @author duongnt.is@gmail.com
 */
public class DefaultLink implements Link {
	private String url;
	private Optional<String> title;
	private Optional<LocalDateTime> version;
	
	public DefaultLink(final String url, final String title, final LocalDateTime version) {
		this.url = url;
		this.title = Optional.ofNullable(title);
		this.version = Optional.ofNullable(version);
	}
	
	public DefaultLink(String url, Optional<String> title, Optional<LocalDateTime> version) {
		this.url = url;
		this.title = title;
		this.version = version;
	}

	@Override
	public String getUrl() {
		return url;
	}

	@Override
	public Optional<String> getTitle() {
		return title;
	}

	@Override
	public Optional<LocalDateTime> version() {
		return version;
	}

	@Override
	public String toString() {
		return "DefaultLink [url=" + url + ", title=" + title + ", version=" + version + "]";
	}

}
