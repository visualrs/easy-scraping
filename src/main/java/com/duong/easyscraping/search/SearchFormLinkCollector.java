package com.duong.easyscraping.search;

import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Optional;

import org.jsoup.Connection;
import org.jsoup.Connection.Method;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.duong.easyscraping.Link;
import com.duong.easyscraping.LinkCollector;
import com.duong.easyscraping.search.profile.ItemExtractor;
import com.duong.easyscraping.search.profile.SearchFormProfile;
import com.duong.easyscraping.utilities.Assert;
import com.duong.easyscraping.utilities.Documents;
import com.duong.easyscraping.utilities.Texts;

/**
 * @author duongnt.is@gmail.com
 */
public class SearchFormLinkCollector implements LinkCollector {
	private static Logger LOGGER = LoggerFactory.getLogger(SearchFormLinkCollector.class);

	private final SearchFormProfile profile;

	private Document currDoc;
	private int currPage = 0;
	private final DateTimeFormatter dateFormat;

	public SearchFormLinkCollector(SearchFormProfile profile) {
		this.profile = profile;
		
		if (Texts.isNotBlank(profile.getItemExtractor().getSelectDate())) {
			dateFormat = DateTimeFormatter.ofPattern(profile.getItemExtractor().getDateFormat());
		} else {
			dateFormat = null;
		}
	}

	public Collection<Link> next() {
		if (!hasNext()) {
			throw new IllegalStateException("This search form has reached its end");
		}

		currDoc = fetchNextDoc();
		
		ItemExtractor itemExtractor = profile.getItemExtractor();
		Elements elements = currDoc.body().select(itemExtractor.getSelectItem());
		
		Collection<Link> links = new ArrayList<>(elements.size());
		
		for (Element element : elements)
        {
            String itemUrl = Documents.extract(element, itemExtractor.getSelectUrl());
            Optional<String> title = Optional.empty();
            Optional<LocalDateTime> version = Optional.empty();
            
            if (Texts.isNotBlank(itemExtractor.getSelectTitle())) {
            	String titleRaw = Documents.extract(element, itemExtractor.getSelectTitle());
            	if (Texts.isNotBlank(titleRaw)) {
            		title = Optional.of(titleRaw);
            	}
            }
            if (Texts.isNotBlank(itemExtractor.getSelectDate())) {
            	String dateString = Documents.extract(element, itemExtractor.getSelectDate());
            	if (Texts.isNotBlank(dateString)) {
            		version = Optional.of(LocalDateTime.parse(dateString, dateFormat));
            	}
            }
            
            links.add(new DefaultLink(itemUrl, title, version));
        }

		return links;
	}

	private Document fetchNextDoc() {
		String url;
		if (currDoc == null) {
			url = profile.getUrl();
		} else {
			url = Documents.extract(currDoc, profile.getSelectNextUrl());
		}
		Assert.hasText(url);
		
		LOGGER.info("Visiting {}", url);
		try {
			Connection connection = Jsoup.connect(url)
									.userAgent("Mozilla")
									.method(Method.valueOf(profile.getMethod()));
			if (profile.getCookies() != null) {
				connection = connection.cookies(profile.getCookies());
			}
			if (profile.getParams() != null) {
				connection = connection.data(profile.getParams());
			}
			Document doc = connection
					.execute()
					.parse();
			
			currPage ++;
			return doc;
		} catch (IOException e) {
			throw new RuntimeException("Error receiving " + url, e);
		}
	}

	public boolean hasNext() {
		if (currDoc != null) {
			return currPage < profile.getMaxPage()
					&& Texts.isNotEmpty(Documents.extract(currDoc, profile.getSelectNextUrl()));
		}
		return true;
	}
}
