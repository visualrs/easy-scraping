package com.duong.easyscraping.search.profile;

import java.util.Map;

/**
 * @author duongnt.is@gmail.com
 */
public class SearchFormProfile {
	private String url;
	private String method;
	private Map<String, String> cookies;
	private Map<String, String> params;

	private ItemExtractor itemExtractor;

	private String selectNextUrl;
	
	private int maxPage = Integer.MAX_VALUE;
	private int maxLink = Integer.MAX_VALUE;

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}
	
	public String getMethod() {
		return method;
	}

	public void setMethod(String method) {
		this.method = method;
	}

	public Map<String, String> getCookies() {
		return cookies;
	}

	public void setCookies(Map<String, String> cookies) {
		this.cookies = cookies;
	}

	public Map<String, String> getParams() {
		return params;
	}

	public void setParams(Map<String, String> params) {
		this.params = params;
	}

	public ItemExtractor getItemExtractor() {
		return itemExtractor;
	}

	public void setItemExtractor(ItemExtractor itemExtractor) {
		this.itemExtractor = itemExtractor;
	}

	public String getSelectNextUrl() {
		return selectNextUrl;
	}

	public void setSelectNextUrl(String selectNextUrl) {
		this.selectNextUrl = selectNextUrl;
	}

	public int getMaxPage() {
		return maxPage;
	}

	public void setMaxPage(int maxPage) {
		this.maxPage = maxPage;
	}

	public int getMaxLink() {
		return maxLink;
	}

	public void setMaxLink(int maxLink) {
		this.maxLink = maxLink;
	}

}
