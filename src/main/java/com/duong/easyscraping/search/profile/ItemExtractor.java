package com.duong.easyscraping.search.profile;

/**
 * @author duongnt.is@gmail.com
 */
public class ItemExtractor {
	private String selectItem;
	private String selectUrl;
	private String selectTitle;
	private String selectDate;
	private String dateFormat;

	public String getSelectItem() {
		return selectItem;
	}

	public void setSelectItem(String selectItem) {
		this.selectItem = selectItem;
	}
	
	public String getSelectUrl() {
		return selectUrl;
	}
	
	public void setSelectUrl(String selectUrl) {
		this.selectUrl = selectUrl;
	}

	public String getSelectTitle() {
		return selectTitle;
	}

	public void setSelectTitle(String selectTitle) {
		this.selectTitle = selectTitle;
	}

	public String getSelectDate() {
		return selectDate;
	}

	public void setSelectDate(String selectDate) {
		this.selectDate = selectDate;
	}

	public String getDateFormat() {
		return dateFormat;
	}

	public void setDateFormat(String dateFormat) {
		this.dateFormat = dateFormat;
	}

}
