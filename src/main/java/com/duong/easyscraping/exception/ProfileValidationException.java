package com.duong.easyscraping.exception;

/**
 * This is for profile validation.
 *
 * @author duongnt.is@gmail.com
 */
public class ProfileValidationException extends RuntimeException {
}
