package com.duong.easyscraping.exception;

/**
 * This will be the root of all errors happen during scraping.
 * This exception is checked, that means, it should be handled immediately to avoid breaking the whole scraping process.
 *
 * @author duongnt.is@gmail.com
 */
public class ScrapingException extends Exception {
    public ScrapingException() {
    }

    public ScrapingException(String message) {
        super(message);
    }

    public ScrapingException(String message, Throwable cause) {
        super(message, cause);
    }

    public ScrapingException(Throwable cause) {
        super(cause);
    }

    public ScrapingException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
