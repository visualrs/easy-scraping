package com.duong.easyscraping.exception;

/**
 * This is for errors related to connection to get the pages to scrap.
 *
 * @author duongnt.is@gmail.com
 */
public class ScrapingConnectionException extends ScrapingException {
    public ScrapingConnectionException() {
    }

    public ScrapingConnectionException(String message) {
        super(message);
    }

    public ScrapingConnectionException(String message, Throwable cause) {
        super(message, cause);
    }

    public ScrapingConnectionException(Throwable cause) {
        super(cause);
    }

    public ScrapingConnectionException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
