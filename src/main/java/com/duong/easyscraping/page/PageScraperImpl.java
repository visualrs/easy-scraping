package com.duong.easyscraping.page;

import java.io.IOException;

import com.duong.easyscraping.exception.ProfileValidationException;
import com.duong.easyscraping.exception.ScrapingConnectionException;
import com.duong.easyscraping.exception.ScrapingException;
import com.duong.easyscraping.page.processor.TypeProcessorRegistry;
import com.duong.easyscraping.page.profile.PropertyMapping;
import com.duong.easyscraping.page.profile.PageObjectMappingProfile;
import com.duong.easyscraping.page.profile.Type;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import com.duong.easyscraping.PageScraper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 
 * @author duongnt.is@gmail.com
 */
public class PageScraperImpl implements PageScraper {
	private static final Logger LOGGER = LoggerFactory.getLogger(PageScraperImpl.class);

	private final PageObjectMappingProfile profile;
	// TODO make it injectable rather then new.
	private TypeProcessorRegistry processorRegistry = new TypeProcessorRegistry();
	private Gson gson = new Gson();

	public PageScraperImpl(PageObjectMappingProfile profile) throws ProfileValidationException {
		this.profile = profile;
	}

	public JsonElement scrapInternal(String url) throws ScrapingException {
		try {
			LOGGER.info("Visiting {}", url);
			Document document = Jsoup.connect(url).userAgent("Mozilla").get();
			PropertyMapping root = new PropertyMapping();
			root.setContent(profile);
			return processorRegistry.getByType(Type.Object).get().process(document, root);
		} catch (IOException e) {
			LOGGER.error("Error connecting " + url, e);
			throw new ScrapingConnectionException("Error connecting " + url, e);
		}
	}

	@Override
	public String scrap(String url) throws ScrapingException {
		return scrapInternal(url).toString();
	}

	@Override
	public <T> T scrap(String url, Class<T> clazz) throws ScrapingException {
		return gson.fromJson(scrapInternal(url), clazz);
	}
}
