package com.duong.easyscraping.page.profile;

/**
 * 
 * @author duongnt.is@gmail.com
 */
public enum Type {
	String,
	Number,
	Boolean,
	Array,
	Object
}
