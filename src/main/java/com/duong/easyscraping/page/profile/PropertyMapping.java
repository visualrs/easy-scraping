package com.duong.easyscraping.page.profile;

import java.util.Objects;

/**
 * 
 * @author duongnt.is@gmail.com
 */
public class PropertyMapping {
	private Type type;
	private String name;
	private String selector;
	private String postScript;

	/**
	 * Mandatory if this is an array property.
	 * 
	 */
	private Type itemType;

	/**
	 * Mandatory if this is an object property.
	 */
	private PageObjectMappingProfile content;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSelector() {
		return selector;
	}

	public void setSelector(String selector) {
		this.selector = selector;
	}

	public String getPostScript() {
		return postScript;
	}

	public void setPostScript(String postScript) {
		this.postScript = postScript;
	}

	public Type getItemType() {
		return itemType;
	}

	public void setItemType(Type itemType) {
		this.itemType = itemType;
	}

	public PageObjectMappingProfile getContent() {
		return content;
	}

	public void setContent(PageObjectMappingProfile content) {
		this.content = content;
	}

	public Type getType() {
		return type;
	}

	public void setType(Type type) {
		this.type = type;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		PropertyMapping that = (PropertyMapping) o;
		return type == that.type &&
				Objects.equals(name, that.name) &&
				Objects.equals(selector, that.selector) &&
				Objects.equals(postScript, that.postScript) &&
				itemType == that.itemType &&
				Objects.equals(content, that.content);
	}

	@Override
	public int hashCode() {
		return Objects.hash(type, name, selector, postScript, itemType, content);
	}
}
