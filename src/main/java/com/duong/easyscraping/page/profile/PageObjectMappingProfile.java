package com.duong.easyscraping.page.profile;

import java.util.List;
import java.util.Objects;

/**
 * 
 * @author duongnt.is@gmail.com
 */
public class PageObjectMappingProfile {
	public PageObjectMappingProfile(List<PropertyMapping> properties) {
		this.properties = properties;
	}

	public PageObjectMappingProfile() {
	}

	private List<PropertyMapping> properties;

	public List<PropertyMapping> getProperties() {
		return properties;
	}

	public void setProperties(List<PropertyMapping> properties) {
		this.properties = properties;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		PageObjectMappingProfile that = (PageObjectMappingProfile) o;
		return Objects.equals(properties, that.properties);
	}

	@Override
	public int hashCode() {
		return Objects.hash(properties);
	}
}
