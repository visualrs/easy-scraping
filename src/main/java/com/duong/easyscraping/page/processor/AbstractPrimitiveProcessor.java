package com.duong.easyscraping.page.processor;

import org.jsoup.helper.StringUtil;
import org.jsoup.nodes.Element;

import com.duong.easyscraping.page.profile.PropertyMapping;
import com.duong.easyscraping.utilities.Documents;
import com.duong.easyscraping.utilities.Scriptings;
import com.google.gson.JsonElement;

public abstract class AbstractPrimitiveProcessor extends AbstractTypeProcessor {

	public AbstractPrimitiveProcessor(TypeProcessorRegistry registry) {
		super(registry);
	}
	
	@Override
	public JsonElement process(Element root, PropertyMapping property) {
		String value = Documents.extract(root, property.getSelector());

		return processRawValue(property, value);
	}

	protected JsonElement processRawValue(PropertyMapping property, String value) {
		if (!StringUtil.isBlank(property.getPostScript())) {
			value = Scriptings.runScript(value, property.getPostScript());
		}
		
		return toPrimitive(value);
	}

	protected abstract JsonElement toPrimitive(String value);
}
