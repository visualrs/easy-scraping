package com.duong.easyscraping.page.processor;

import com.google.gson.JsonNull;
import org.jsoup.nodes.Element;

import com.duong.easyscraping.page.profile.PropertyMapping;
import com.duong.easyscraping.page.profile.Type;
import com.duong.easyscraping.utilities.Texts;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

public class ObjectProcessor extends AbstractTypeProcessor {
	
	public ObjectProcessor(TypeProcessorRegistry registry) {
		super(registry);
	}

	@Override
	public Type target() {
		return Type.Object;
	}

	@Override
	public JsonElement process(final Element root, PropertyMapping property) {
		Element objectRoot = Texts.isNotBlank(property.getSelector()) ? root.select(property.getSelector()).first() : root;

		if (objectRoot == null) {
			return JsonNull.INSTANCE;
		}
		JsonObject object = new JsonObject();
		
		property.getContent().getProperties().forEach(subProperty -> {
			JsonElement element = registry().getByType(subProperty.getType()).get().process(objectRoot, subProperty);
			object.add(subProperty.getName(), element);
		});
		
		return object;
	}

}
