package com.duong.easyscraping.page.processor;

import com.duong.easyscraping.page.profile.Type;

import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

/**
 * 
 * @author duongnt.is@gmail.com
 */
public class TypeProcessorRegistry {
	private ConcurrentMap<Type, TypeProcessor> registry = new ConcurrentHashMap<>();

	public TypeProcessorRegistry() {
		register(new ObjectProcessor(this));
		register(new BooleanProcessor(this));
		register(new NumberProcessor(this));
		register(new StringProcessor(this));
		register(new ArrayProcessor(this));
	}

	public Optional<TypeProcessor> getByType(Type type) {
		return Optional.ofNullable(registry.get(type));
	}
	
	public void register(TypeProcessor processor) {
		TypeProcessor ret = registry.putIfAbsent(processor.target(), processor);
		if (ret != null) {
			throw new IllegalStateException("There is already another processor of this type " + processor.target());
		}
	}
}
