package com.duong.easyscraping.page.processor;

import com.duong.easyscraping.page.profile.Type;
import com.duong.easyscraping.utilities.Texts;
import com.google.gson.JsonElement;
import com.google.gson.JsonNull;
import com.google.gson.JsonPrimitive;

public class BooleanProcessor extends AbstractPrimitiveProcessor {
	
	public BooleanProcessor(TypeProcessorRegistry registry) {
		super(registry);
	}

	@Override
	public Type target() {
		return Type.Boolean;
	}

	@Override
	protected JsonElement toPrimitive(String value) {
		return Texts.isNotBlank(value) ? new JsonPrimitive(Boolean.valueOf(value)) : JsonNull.INSTANCE;
	}

}
