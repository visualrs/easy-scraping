package com.duong.easyscraping.page.processor;

import com.duong.easyscraping.page.profile.Type;
import com.google.gson.JsonElement;
import com.google.gson.JsonNull;
import com.google.gson.JsonPrimitive;

public class StringProcessor extends AbstractPrimitiveProcessor {
	
	public StringProcessor(TypeProcessorRegistry registry) {
		super(registry);
	}

	@Override
	public Type target() {
		return Type.String;
	}

	@Override
	protected JsonElement toPrimitive(String value) {
		return value != null ? new JsonPrimitive(value) : JsonNull.INSTANCE;
	}

}
