package com.duong.easyscraping.page.processor;

import org.jsoup.nodes.Element;

import com.duong.easyscraping.page.profile.PropertyMapping;
import com.duong.easyscraping.page.profile.Type;
import com.google.gson.JsonElement;

/**
 * This is an internal interfaces, should not be use outside the library.
 * External client will use PageScraper instead.
 *
 * @author duongnt.is@gmail.com
 */
public interface TypeProcessor {
	Type target();
	JsonElement process(Element root, PropertyMapping property);
}
