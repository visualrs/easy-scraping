package com.duong.easyscraping.page.processor;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import com.duong.easyscraping.page.profile.PropertyMapping;
import com.duong.easyscraping.page.profile.Type;
import com.duong.easyscraping.utilities.Documents;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;

public class ArrayProcessor extends AbstractTypeProcessor {

	public ArrayProcessor(TypeProcessorRegistry registry) {
		super(registry);
	}

	@Override
	public Type target() {
		return Type.Array;
	}

	@Override
	public JsonElement process(Element root, PropertyMapping property) {
		final TypeProcessor itemProcessor = registry().getByType(property.getItemType()).get();
		
		final List<JsonElement> jsonElements;
		final PropertyMapping itemProperty = new PropertyMapping();
		itemProperty.setType(property.getItemType());
		itemProperty.setContent(property.getContent());
		itemProperty.setPostScript(property.getPostScript());
		
		if (property.getItemType() == Type.Object) {
			Elements elements = root.select(property.getSelector());
			
			jsonElements = elements.stream()
										.map(e -> itemProcessor.process(e, itemProperty))
										.collect(Collectors.toList());
		} else { // itemType is a primitive type.
			Collection<String> childs = Documents.extractAll(root, property.getSelector());
			AbstractPrimitiveProcessor primitiveProcessor = (AbstractPrimitiveProcessor) itemProcessor;
			
			jsonElements = childs.stream()
							.map(rawValue -> primitiveProcessor.processRawValue(itemProperty, rawValue))
							.collect(Collectors.toList());
		}
		JsonArray jsonArray = new JsonArray();
		jsonElements.forEach(jsonArray::add);
		
		return jsonArray;
	}

}
