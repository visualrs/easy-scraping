package com.duong.easyscraping.page.processor;

public abstract class AbstractTypeProcessor implements TypeProcessor {
	private TypeProcessorRegistry registry;
	
	public AbstractTypeProcessor(TypeProcessorRegistry registry) {
		this.registry = registry;
	}

	protected TypeProcessorRegistry registry() {
		return registry;
	}
}
