package com.duong.easyscraping;

import java.io.InputStream;

import org.yaml.snakeyaml.Yaml;

import com.duong.easyscraping.search.SearchFormLinkCollector;
import com.duong.easyscraping.search.profile.SearchFormProfile;

/**
 * @author duongnt.is@gmail.com
 */
public final class LinkCollectors {
	private LinkCollectors() {}
	
	public static LinkCollector fromYaml(InputStream is) {
		Yaml yaml = new Yaml();
		SearchFormProfile profile = yaml.loadAs(is, SearchFormProfile.class);
		return new SearchFormLinkCollector(profile);
	}

	public static LinkCollector fromProfile(SearchFormProfile profile) {
		return new SearchFormLinkCollector(profile);
	}
}
