package com.duong.easyscraping;

import com.duong.easyscraping.exception.ProfileValidationException;
import com.duong.easyscraping.page.PageScraperImpl;
import com.duong.easyscraping.page.profile.PageObjectMappingProfile;
import org.yaml.snakeyaml.Yaml;

import java.io.InputStream;

/**
 * @author duongnt.is@gmail.com
 */
public final class PageScrapers {
    private PageScrapers() {}

    public static PageScraper fromYaml(InputStream is) throws ProfileValidationException {
        Yaml yaml = new Yaml();
        PageObjectMappingProfile profile = yaml.loadAs(is, PageObjectMappingProfile.class);
        return fromProfile(profile);
    }

    public static PageScraper fromProfile(PageObjectMappingProfile profile) throws ProfileValidationException {
        return new PageScraperImpl(profile);
    }
}
