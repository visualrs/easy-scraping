package com.duong.easyscraping;

import java.util.Collection;

/**
 * Entry point: Collect links from search page.
 * 
 * Important notes: This is not designed for thread-safety as it has to mode from page to next page one by one.
 * 
 * @author duongnt.is@gmail.com
 */
public interface LinkCollector {
	
	/**
	 * Collect next chunk of links, normally from the next page.
	 * 
	 * @return chunks of links.
	 */
	Collection<Link> next();
	
	/**
	 * Check if this collector does reach it ends.
	 * 
	 * After the collector is created (no any request made yet), this always return true. 
	 * 
	 * @return true if the collector can move to collect next chunk links.
	 */
	boolean hasNext();
}
