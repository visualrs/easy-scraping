package com.duong.easyscraping.search;

import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import javax.ws.rs.core.UriBuilder;

import com.duong.easyscraping.ServerTest;
import org.glassfish.jersey.jdkhttp.JdkHttpServerFactory;
import org.glassfish.jersey.server.ResourceConfig;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.yaml.snakeyaml.DumperOptions;
import org.yaml.snakeyaml.Yaml;

import com.duong.easyscraping.Link;
import com.duong.easyscraping.LinkCollector;
import com.duong.easyscraping.LinkCollectors;
import com.duong.easyscraping.search.profile.ItemExtractor;
import com.duong.easyscraping.search.profile.SearchFormProfile;
import com.sun.net.httpserver.HttpServer;

import static org.hamcrest.Matchers.*;
import static org.junit.Assert.*;

@SuppressWarnings("restriction")
public class SearchFormLinkCollectorTest {
	private static final String TEST_SEARCH_FORM_OK = "search/manga-net-search-form-ok.yaml";
	private static final String TEST_SEARCH_FORM_500 = "search/manga-net-search-form-500.yaml";
	private static HttpServer server;
	
	@Rule
	public ExpectedException expectedExec = ExpectedException.none();

	@BeforeClass
	public static void setUp() {
		URI uri = UriBuilder.fromUri("http://127.0.0.1:8181/test/").build();
		ResourceConfig resourceConfig = new ResourceConfig(ServerTest.class);
		server = JdkHttpServerFactory.createHttpServer(uri, resourceConfig);
	}

	@SuppressWarnings("restriction")
	@AfterClass
	public static void cleanUp() {
		server.stop(0);
	}

	@Test
	public void testCollectLinksOK() throws IOException {
		LinkCollector linkCollector = getCollectorFromFile(TEST_SEARCH_FORM_OK);

		// First page
		assertThat(linkCollector.hasNext(), is(true));
		Collection<Link> links = linkCollector.next();
		assertThat(links.size(), is(45));

		// Assert first
		Link first = links.stream().findFirst().get();
		assertThat(first.getTitle(), is(Optional.of("Renai Boukun")));
		assertThat(first.getUrl(), is("http://mangak.net/renai-boukun/"));

		// Assert last
		Link last = links.stream().reduce(null, (i1, i2) -> i2);
		assertThat(last.getTitle(), is(Optional.of("Blade Notes")));
		assertThat(last.getUrl(), is("http://mangak.net/blade-notes/"));

		// Try next page
		assertThat(linkCollector.hasNext(), is(true));
		links = linkCollector.next();
		assertThat(links.size(), is(45));

		// Assert first
		first = links.stream().findFirst().get();
		assertThat(first.getTitle(), is(Optional.of("Imawa no Kuni no Alice")));
		assertThat(first.getUrl(), is("http://mangak.net/imawa-no-kuni-no-alice/"));

		// Assert last
		last = links.stream().reduce(null, (i1, i2) -> i2);
		assertThat(last.getTitle(), is(Optional.of("World Destruction")));
		assertThat(last.getUrl(), is("http://mangak.net/world-destruction/"));

		assertThat(linkCollector.hasNext(), is(true));
	}
	
	@Test
	public void testCollectLinks500() throws IOException {
		LinkCollector linkCollector = getCollectorFromFile(TEST_SEARCH_FORM_500);
		// We don't know what will happen yet, so always say 'yes'.
		assertThat(linkCollector.hasNext(), is(true));
		
		expectedExec.expect(RuntimeException.class);
		expectedExec.expectMessage("Error receiving");
		linkCollector.next();
	}
	
	private LinkCollector getCollectorFromFile(String yamlFile) throws IOException {
		try (InputStream is = this.getClass().getClassLoader().getResourceAsStream(yamlFile)) {
			return LinkCollectors.fromYaml(is);
		}
	}

	@Test
	@Ignore
	public void testLayoutProfile() throws Exception {
		SearchFormProfile profile = new SearchFormProfile();

		Map<String, String> cookies = new HashMap<>();
		cookies.put("example", "dsads%$sdsadsa");
		profile.setCookies(cookies);
		profile.setUrl("http://example.com/search?quersa#");
		Map<String, String> params = new HashMap<>();
		params.put("param1", "ssad sadsa sdsa");
		profile.setParams(params);

		profile.setMethod("GET");
		profile.setSelectNextUrl("div.seach-result > div#controls > a#next >> abs:href");

		ItemExtractor extractor = new ItemExtractor();
		extractor.setSelectItem("div.search-productItem");
		extractor.setSelectTitle(".p-title > a >> abs:text");
		extractor.setSelectUrl(".p-title > a >> abs:href");
		extractor.setSelectDate(".p-bottom-crop > .floatright");
		extractor.setDateFormat("dd/MM/yyyy");
		profile.setItemExtractor(extractor);

		DumperOptions options = new DumperOptions();
		options.setPrettyFlow(true);
		Yaml yaml = new Yaml(options);
		System.out.println(yaml.dump(profile));
	}
}
