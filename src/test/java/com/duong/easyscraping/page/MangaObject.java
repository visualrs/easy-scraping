package com.duong.easyscraping.page;

import java.util.List;

/**
 * Sample object to test json mapping support.
 * @author duongnt.is@gmail.com
 */
public class MangaObject {
    private String title;
    private String author;
    private int viewNum;
    private boolean isFinish;
    private List<Chapter> chapterList;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public int getViewNum() {
        return viewNum;
    }

    public void setViewNum(int viewNum) {
        this.viewNum = viewNum;
    }

    public boolean isFinish() {
        return isFinish;
    }

    public void setFinish(boolean finish) {
        isFinish = finish;
    }

    public List<Chapter> getChapterList() {
        return chapterList;
    }

    public void setChapterList(List<Chapter> chapterList) {
        this.chapterList = chapterList;
    }
}
