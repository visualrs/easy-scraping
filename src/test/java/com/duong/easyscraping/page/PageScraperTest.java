package com.duong.easyscraping.page;

import com.duong.easyscraping.PageScraper;
import com.duong.easyscraping.PageScrapers;
import com.duong.easyscraping.ServerTest;
import com.duong.easyscraping.exception.ScrapingException;
import com.duong.easyscraping.page.profile.PageObjectMappingProfile;
import com.duong.easyscraping.page.profile.PropertyMapping;
import com.duong.easyscraping.page.profile.Type;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.sun.net.httpserver.HttpServer;
import org.glassfish.jersey.jdkhttp.JdkHttpServerFactory;
import org.glassfish.jersey.server.ResourceConfig;
import org.junit.*;
import org.junit.rules.ExpectedException;
import org.yaml.snakeyaml.DumperOptions;
import org.yaml.snakeyaml.Yaml;

import javax.ws.rs.core.UriBuilder;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static com.duong.easyscraping.ServerTest.PAGE_ERROR;
import static com.duong.easyscraping.ServerTest.PAGE_SAMPLE;
import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.assertThat;

/**
 * @author duongnt.is@gmail.com
 */
public class PageScraperTest {
    public static final String BASE_PATH = "http://127.0.0.1:8181/";
    private static HttpServer server;

    private JsonParser jsonParser = new JsonParser();
    @Rule
    public ExpectedException expectedExec = ExpectedException.none();

    @BeforeClass
    public static void setUp() {
        URI uri = UriBuilder.fromUri(BASE_PATH).build();
        ResourceConfig resourceConfig = new ResourceConfig(ServerTest.class);
        server = JdkHttpServerFactory.createHttpServer(uri, resourceConfig);
    }

    @SuppressWarnings("restriction")
    @AfterClass
    public static void cleanUp() {
        if (server != null) {
            server.stop(0);
        }
    }

    @Test
    public void testSimpleScrap() throws IOException, ScrapingException {
        PageScraper pageScraper = getCollectorFromFile("page/simple_profile.yaml");

        String json = pageScraper.scrap(BASE_PATH + PAGE_SAMPLE);
        JsonObject jsonObject = jsonParser.parse(json).getAsJsonObject();

        System.out.println(json);

        assertThat(jsonObject.get("title").getAsString(), is("The Breaker – Kẻ phá hoại"));
        assertThat(jsonObject.get("author").getAsString(), is("JEON Geuk-jin"));
        assertThat(jsonObject.get("isFinish").getAsBoolean(), is(true));
        assertThat(jsonObject.get("viewNum").getAsInt(), is(26759));
    }

    @Test
    public void testPlainArrayScrap() throws IOException, ScrapingException {
        PageScraper pageScraper = getCollectorFromFile("page/plain_array_profile.yaml");

        String json = pageScraper.scrap(BASE_PATH + PAGE_SAMPLE);
        JsonObject jsonObject = jsonParser.parse(json).getAsJsonObject();

        System.out.println(json);

        assertThat(jsonObject.get("title").getAsString(), is("The Breaker – Kẻ phá hoại"));
        JsonArray chapterList = jsonObject.get("chapterList").getAsJsonArray();
        assertThat(chapterList, notNullValue());
        assertThat(chapterList.size(), is(72));
        assertThat(chapterList.get(0).getAsString(), is("http://mangak.net/the-breaker-ke-pha-hoai-chap-72/"));
        assertThat(chapterList.get(71).getAsString(), is("http://mangak.net/the-breaker-ke-pha-hoai-chap-1/"));
    }

    @Test
    public void testSubObjectScrap() throws IOException, ScrapingException {
        PageScraper pageScraper = getCollectorFromFile("page/sub_object_profile.yaml");

        String json = pageScraper.scrap(BASE_PATH + PAGE_SAMPLE);
        JsonObject jsonObject = jsonParser.parse(json).getAsJsonObject();

        System.out.println(json);

        assertThat(jsonObject.get("description").getAsString(), startsWith("<h2>The Breaker – Kẻ phá hoại Full nội"));

        JsonObject details = jsonObject.getAsJsonObject("details");
        assertThat(details.get("title").getAsString(), is("The Breaker – Kẻ phá hoại"));
        assertThat(details.get("author").getAsString(), is("JEON Geuk-jin"));
        assertThat(details.get("isFinish").getAsBoolean(), is(true));
        assertThat(details.get("viewNum").getAsInt(), is(26759));
    }

    @Test
    public void testArrayOfObjectsScrap() throws IOException, ScrapingException {
        PageScraper pageScraper = getCollectorFromFile("page/objects_array_profile.yaml");

        String json = pageScraper.scrap(BASE_PATH + PAGE_SAMPLE);
        JsonObject jsonObject = jsonParser.parse(json).getAsJsonObject();

        System.out.println(json);

        assertThat(jsonObject.get("title").getAsString(), is("The Breaker – Kẻ phá hoại"));

        JsonArray chapterList = jsonObject.get("chapterList").getAsJsonArray();
        assertThat(chapterList, notNullValue());
        assertThat(chapterList.size(), is(72));
        assertThat(chapterList.get(0).getAsJsonObject().get("title").getAsString(), is("The Breaker – Kẻ phá hoại chap 72"));
        assertThat(chapterList.get(0).getAsJsonObject().get("url").getAsString(), is("http://mangak.net/the-breaker-ke-pha-hoai-chap-72/"));
        assertThat(chapterList.get(0).getAsJsonObject().get("date").getAsString(), is("09-08-2014"));
        assertThat(chapterList.get(71).getAsJsonObject().get("title").getAsString(), is("The Breaker – Kẻ phá hoại chap 1"));
        assertThat(chapterList.get(71).getAsJsonObject().get("url").getAsString(), is("http://mangak.net/the-breaker-ke-pha-hoai-chap-1/"));
        assertThat(chapterList.get(71).getAsJsonObject().get("date").getAsString(), is("18-03-2014"));
    }

    @Test
    public void testScrapToObject() throws IOException, ScrapingException {
        PageScraper pageScraper = getCollectorFromFile("page/objects_array_profile.yaml");

        MangaObject mangaObject = pageScraper.scrap(BASE_PATH + PAGE_SAMPLE, MangaObject.class);

        assertThat(mangaObject.getTitle(), is("The Breaker – Kẻ phá hoại"));

        List<Chapter> chapterList = mangaObject.getChapterList();
        assertThat(chapterList, notNullValue());
        assertThat(chapterList.size(), is(72));
        assertThat(chapterList.get(0).getTitle(), is("The Breaker – Kẻ phá hoại chap 72"));
        assertThat(chapterList.get(0).getUrl(), is("http://mangak.net/the-breaker-ke-pha-hoai-chap-72/"));
        assertThat(chapterList.get(0).getDate(), is("09-08-2014"));
        assertThat(chapterList.get(71).getTitle(), is("The Breaker – Kẻ phá hoại chap 1"));
        assertThat(chapterList.get(71).getUrl(), is("http://mangak.net/the-breaker-ke-pha-hoai-chap-1/"));
        assertThat(chapterList.get(71).getDate(), is("18-03-2014"));
    }

    @Test
    public void testScrapWithNonExistingProps() throws IOException, ScrapingException {
        PageScraper pageScraper = getCollectorFromFile("page/with_nonexist_props_profile.yaml");

        String json = pageScraper.scrap(BASE_PATH + PAGE_SAMPLE);
        JsonObject jsonObject = jsonParser.parse(json).getAsJsonObject();

        System.out.println(json);

        assertThat(jsonObject.get("title").getAsString(), is("The Breaker – Kẻ phá hoại"));
        assertThat(jsonObject.get("author").isJsonNull(), is(true));
        assertThat(jsonObject.get("isFinish").getAsBoolean(), is(false));
        assertThat(jsonObject.get("viewNum").isJsonNull(), is(true));

        JsonObject details = jsonObject.getAsJsonObject("subPropNotFound");
        assertThat(details, notNullValue());
        assertThat(details.get("foundProp").getAsString(), is("JEON Geuk-jin"));
        assertThat(details.get("notFoundProp").isJsonNull(), is(true));

        assertThat(jsonObject.get("wholeObjectNotFound").isJsonNull(), is(true));
        assertThat(jsonObject.get("notFoundArray").getAsJsonArray().size(), is(0));
    }

    @Test
    public void testScrapError() throws IOException, ScrapingException {
        PageScraper pageScraper = getCollectorFromFile("page/simple_profile.yaml");
        expectedExec.expect(ScrapingException.class);
        expectedExec.expectMessage("Error connecting " + BASE_PATH + PAGE_ERROR);
        
        pageScraper.scrap(BASE_PATH + PAGE_ERROR);

    }

    private PageScraper getCollectorFromFile(String yamlFile) throws IOException {
        try (InputStream is = this.getClass().getClassLoader().getResourceAsStream(yamlFile)) {
            return PageScrapers.fromYaml(is);
        }
    }

    @Test
    @Ignore
    public void testProfileLayout() {
        List<PropertyMapping> properties = new ArrayList<>();

        PropertyMapping property = new PropertyMapping();
        property.setType(Type.String);
        property.setSelector("xxx >> yyy");
        property.setName("myStringProp");
        properties.add(property);

        property = new PropertyMapping();
        property.setType(Type.Boolean);
        property.setSelector("xxx >> yyy");
        property.setName("myBooleanProp");
        properties.add(property);

        property = new PropertyMapping();
        property.setType(Type.Number);
        property.setSelector("xxx >> yyy");
        property.setName("myNumberProp");
        properties.add(property);

        property = new PropertyMapping();
        property.setType(Type.Array);
        property.setSelector("xxx >> yyy");
        property.setName("myArrayProp");
        property.setItemType(Type.String);
        properties.add(property);

        property = new PropertyMapping();
        property.setType(Type.Object);
        property.setSelector("xxx >> yyy");
        property.setName("myObjectProp");
        PropertyMapping subProperty1 = new PropertyMapping();
        subProperty1.setName("subPropString");
        subProperty1.setType(Type.String);
        property.setContent(new PageObjectMappingProfile(Arrays.asList(subProperty1)));
        properties.add(property);

        property = new PropertyMapping();
        property.setType(Type.Array);
        property.setSelector("xxx >> yyy");
        property.setName("myArrayOfObjectProp");
        property.setItemType(Type.Object);
        subProperty1 = new PropertyMapping();
        subProperty1.setName("subPropString");
        subProperty1.setType(Type.String);
        property.setContent(new PageObjectMappingProfile(Arrays.asList(subProperty1)));

        properties.add(property);

        PageObjectMappingProfile profile = new PageObjectMappingProfile(properties);

        DumperOptions options = new DumperOptions();
        options.setPrettyFlow(true);
        Yaml yaml = new Yaml(options);
        System.out.println(yaml.dump(profile));
    }
}
