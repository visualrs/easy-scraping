package com.duong.easyscraping.page;

/**
 * Sample object to test json mapping support.
 * @author duongnt.is@gmail.com
 */
public class Chapter {
    private String url;
    private String title;
    private String date;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
