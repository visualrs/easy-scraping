package com.duong.easyscraping;

import java.io.IOException;
import java.io.InputStream;
import java.util.Scanner;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

@Path("/")
@Produces("text/html")
public class ServerTest {

	public static final String PAGE_SAMPLE = "pages/sample";
	public static final String PAGE_ERROR = "pages/500";

	@Path("search")
	@GET
	public Response searchOk() throws IOException {
		return Response.ok(readFileFromClassPath("search/manga-net.html")).build();
	}
	
	@Path("search-2")
	@GET
	public Response searchOkPage2() throws IOException {
		return Response.ok(readFileFromClassPath("search/manga-net-2.html")).build();
	}
	
	@Path("search-error-500")
	@GET
	public Response search500() throws IOException {
		return Response.status(500).build();
	}

	@Path(PAGE_SAMPLE)
	@GET
	public Response samplePage() throws IOException {
		return Response.ok(readFileFromClassPath("page/TheBreaker-Mangak.net.html")).build();
	}

	@Path(PAGE_ERROR)
	@GET
	public Response errorPage() throws IOException {
		return Response.serverError().build();
	}

	private String readFileFromClassPath(String fileName) throws IOException {
		try (InputStream is = this.getClass().getClassLoader().getResourceAsStream(fileName);
				Scanner scanner = new Scanner(is).useDelimiter("\\A");) {
			return scanner.next();
		}
	}
	
}
