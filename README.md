# Overview #
This is a(nother) library that provides easy scraping on websites. Developers can turns out web pages to valuable JSON data by a few lines of code.

# Getting started #
We can start by a comparison to famous `Hibernate`: `Hibernate` turns RDBMS rows into Objects; `easy-scraping` turn a website into valuable JSONs or Objects (with the help of GSON). Does it should cool enough? Let's enjoy it by eyes by looking the code bellow.

```
#!java

PageObjectMappingProfile profile = getScrapingProfile(); // This enables profiles setting at runtime.
PageScraper myScrtapper= PageScrapers.fromProfile();
MyObject myObject = pageScraper.scrap(aUrl, MyObject.class);
```
Or another version, with JSON returned instead of Object:
```
#!java

String myJson = pageScraper.scrap(aUrl);
```

So far, the still something unclear. Where is it? The profile? Yes. WTF is it?
In `Hibernate`, we need to specify a mapping structure: the column `myColumn` is mapped to that Object's property `myProperty` and how should it map. In `easy-scraping`, a profile define, for each expected property, what is the HTML element(s) we should select, which type (JSON types) is the property and if there is any runtime expression needed to customize the value after selected from HTML elements.
A profile should look like bellow:

```
#!yaml

properties:
- {
  type: String,
  name: author,
  selector: "ul.truyen_info_right > li:has(span:contains(Author:)) > a"
}
- {
  type: String,
  name: title,
  selector: "h1.entry-title"
}
- {
  type: String,
  name: status,
  selector: "ul.truyen_info_right > li:has(span:contains(Status :)) > a",
  postScript: "value === 'Finished' ? 'finish' : 'in_progress'",
}
- {
  type: Number,
  name: numOfViews,
  selector: "ul.truyen_info_right > li:has(span:contains(Lượt Xem :))",
  postScript: "value.replace('Number of views :', '').replace(',','')"
}
- name: chapterList
  type: Array
  itemType: Object
  selector: "div.list_chapter > div.chapter-list > div.row"
  content:
      properties:
      - {
        name: scrapUrl,
        type: String,
        selector: "span > a >> abs:href"
      }
      - {
        name: name,
        type: String,
        selector: "span > a"
      }
```

And with that profile, a sample result can be like bellow:

```
#!JSON

{  
   "author":"Hiroya Oku",
   "title":"GantZ: G",
   "status":"in_progress",
   "numOfViews":8778.0,
   "chapterList":[  
      {  
         "scrapUrl":"http://127.0.0.1:8181/gantz-g-chap-2/",
         "name":"GantZ: G chap 2"
      },
      {  
         "scrapUrl":"http://127.0.0.1:8181/gantz-g-chap-1/",
         "name":"GantZ: G chap 1"
      }
   ]
}
```

Have a look at our [integration test](https://bitbucket.org/visualrs/easy-scraping/src/18679744e13e9a0abec4ce2ae883c77719dc6919/src/test/java/com/duong/easyscraping/?at=master) for more use cases.
# Maven integration #
Include `easy-scraping` dependency, e.g.

    <dependency>
        <groupId>com.duong.easyscraping</groupId>
        <artifactId>easy-scraping</artifactId>
        <version>1.0.0</version>
    </dependency>